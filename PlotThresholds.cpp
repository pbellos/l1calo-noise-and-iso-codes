vector<double> Symmetrize(vector<double> V){
    for (int i=0; i<V.size()/2; i++){ if (fabs(V[i])>= fabs(V[V.size()-1-i])) V[i]=fabs(V[V.size()-1-i]);}
    V.erase(V.begin() + V.size() / 2, V.end());
    reverse(V.begin(), V.end());
    return V;}

int MevToCounts(double t){return (t/25) +32;}

vector<vector<double>> MakeThresholds(TString run){

 double a, b,c,d,e,f,g, h, j ;
 vector<pair<double, int>> PS, L1,L2,L3,Had;
 vector<double> PSeta, PSthr, Hadeta, Hadthr, L1eta, L1thr, L2eta, L2thr, L3eta, L3thr;
 vector<vector<double>>  V, VS;
 ifstream infile(run);

 while (!infile.eof()) {

   infile >>a>>b>>c>>d;
   if (fabs(c)>2.5) continue;
   if (a==2) {Had.push_back(make_pair(c,d*1000));}
   if (b==0 && a!=2) {PS.push_back(make_pair(c,d*1000));}
   if (b==1) {L1.push_back(make_pair(c,d*1000));}
   if (b==2) {L2.push_back(make_pair(c,d*1000));}
   if (b==3) {L3.push_back(make_pair(c,d*1000));}
    }

  sort(Had.begin(), Had.end());
  sort(PS.begin(), PS.end());
  sort(L1.begin(), L1.end());
  sort(L2.begin(), L2.end());
  sort(L3.begin(), L3.end());

  std::transform(Had.begin(), Had.end(), std::back_inserter(Hadeta), [](const std::pair<double, int>& pair) { return pair.first; });  V.push_back(Hadeta);
  std::transform(Had.begin(), Had.end(), std::back_inserter(Hadthr), [](const std::pair<double, int>& pair) { return pair.second; }); V.push_back(Hadthr);

  std::transform(PS.begin(), PS.end(), std::back_inserter(PSeta), [](const std::pair<double, int>& pair) { return pair.first; });  V.push_back(PSeta);
  std::transform(PS.begin(), PS.end(), std::back_inserter(PSthr), [](const std::pair<double, int>& pair) { return pair.second; }); V.push_back(PSthr);

  std::transform(L1.begin(), L1.end(), std::back_inserter(L1eta), [](const std::pair<double, int>& pair) { return pair.first; });  V.push_back(L1eta);
  std::transform(L1.begin(), L1.end(), std::back_inserter(L1thr), [](const std::pair<double, int>& pair) { return pair.second; }); V.push_back(L1thr);

  std::transform(L2.begin(), L2.end(), std::back_inserter(L2eta), [](const std::pair<double, int>& pair) { return pair.first; });  V.push_back(L2eta);
  std::transform(L2.begin(), L2.end(), std::back_inserter(L2thr), [](const std::pair<double, int>& pair) { return pair.second; }); V.push_back(L2thr);

  std::transform(L3.begin(), L3.end(), std::back_inserter(L3eta), [](const std::pair<double, int>& pair) { return pair.first; });  V.push_back(L3eta);
  std::transform(L3.begin(), L3.end(), std::back_inserter(L3thr), [](const std::pair<double, int>& pair) { return pair.second; }); V.push_back(L3thr);

 for (int i=0; i<V.size(); i++) VS.push_back(Symmetrize(V[i]));

  for (int i=0; i<VS.size(); i++) {
    if(i%2==0) cout<<endl<<"          --------------        "<<endl; cout<<endl;
    for (int j=0; j<VS[i].size(); j++) {
        if(i%2==0) cout<<setw(5)<<VS[i][j]<<"  ";
        else cout<<setw(5)<<MevToCounts(VS[i][j])<<"  ";
  }}

    cout<<endl;

 return VS;

}


void PlotThresholds () {

 vector<vector<double>> T1 = MakeThresholds("New_run452028_1pc.txt");
 vector<vector<double>> T2 = MakeThresholds("New_run452028_05pc.txt");

///------------------------------------------------------------------------------------------

 TLine *l1 = new TLine(0,800,1.8,800);            // 64
 TLine *l2 = new TLine(0,600,2.5,600);            //48

 TFile * F = new TFile ("test.root","recreate");

 TCanvas *c3 = new TCanvas("c3","c3",600, 400);
 TMultiGraph *mg = new TMultiGraph("mg","mg");

 vector <TGraph *> G;

 for(int i=0; i<5; i++) G.push_back(new TGraph (T1[2*i].size(), &T1[2*i][0] , &T1[2*i+1][0]));
 for(int i=0; i<5; i++) G.push_back(new TGraph (T2[2*i].size(), &T2[2*i][0] , &T2[2*i+1][0]));

 for (int i=0; i<5; i++) G[i]->SetMarkerStyle(21);
 for (int i=5; i<10; i++) G[i]->SetMarkerStyle(20);

 for (int i=0; i<5; i++) G[i]->SetMarkerColor(i+1);
 for (int i=5; i<10; i++) G[i]->SetMarkerColor(i+1-5);

 for (int i=0; i<10; i++) mg->Add(G[i]);

 mg->Draw("AP");
 mg->SetTitle("; |#eta|; MeV");
 l1->Draw("same");
 l1->SetLineColor(kRed);
 l2->Draw("same");
 l2->SetLineColor(kMagenta);
 c3->Write();

 F->Close();
}







/*





  // 0.5% thresholds PM



//   vector<double> m_noisecutPS_eta = {62,62,61,61,60,60,59,59,60,60,60,58,58,57,59,71,72,67, 0, 0, 0, 0, 0, 0,42};
//   vector<double> m_noisecutL1_eta = {46,46,46,46,46,45,44,44,40,39,39,39,38,38,42,39,39,41,39,39,41,42,42,39, 0};
//   vector<double> m_noisecutL2_eta = {50,50,51,50,49,49,48,47,50,49,47,47,45,44,48,45,43,43,52,49,48,47,45,47,46};
//   vector<double> m_noisecutL3_eta = {45,44,45,43,43,43,43,45,52,53,52,53,50,47,47,47,47,46,44,43,42,42,42,42,41};
//   vector<double> m_noisecutHad_eta= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,53,70,70,62,64,61,60,57,56,54};




//   vector<double> m_noisecutPS_eta23 = {66,65,65,65,63,62,62,62,62,61,63,61,64,63,62,64,74,66, 0, 0, 0, 0, 0, 0,44};
//   vector<double> m_noisecutL1_eta23 = {49,49,48,48,48,47,47,46,41,41,41,41,39,39,42,41,41,43,41,41,44,44,45,41, 0};
//   vector<double> m_noisecutL2_eta23 = {53,54,53,53,53,53,52,50,53,52,50,49,49,46,52,47,45,46,54,51,50,49,47,49,48};
//   vector<double> m_noisecutL3_eta23 = {46,44,45,44,44,45,45,45,52,53,51,53,54,46,47,48,48,46,45,44,44,44,45,44,42};
//   vector<double> m_noisecutHad_eta23= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,55,79,77,66,70,68,65,65,63,60};


///run452028_ZB_0.5%//
   vector<double> m_noisecutPS_eta = {67,66,65,64,63,61,63,64,63,59,62,62,62,62,62,73,71,67, 0, 0, 0, 0, 0, 0,45};
   vector<double> m_noisecutL1_eta = {48,49,49,49,48,48,47,46,41,41,41,41,39,39,42,42,41,43,41,41,43,44,44,41, 0};
   vector<double> m_noisecutL2_eta = {53,53,53,52,53,52,51,50,52,52,50,49,48,46,50,47,45,46,54,52,50,48,47,50,48};
   vector<double> m_noisecutL3_eta = {45,45,45,44,44,45,45,46,54,52,53,53,51,45,47,48,48,46,45,44,44,43,44,45,42};
   vector<double> m_noisecutHad_eta= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,58,79,76,70,72,67,68,64,60,61};
//


    ///run452028_ZB_2%//
    vector<double> m_noisecutPS_eta23 = {54,54,54,54,52,52,52,53,51,50,52,51,51,52,50,63,59,56, 0, 0, 0, 0, 0, 0,39};
    vector<double> m_noisecutL1_eta23 = {42,42,42,42,41,41,40,39,38,37,37,37,37,36,39,38,37,38,37,37,38,38,38,37,0 };
    vector<double> m_noisecutL2_eta23 = {44,45,44,44,45,43,43,43,43,42,41,40,39,39,44,42,40,40,48,45,45,43,42,43,42};
    vector<double> m_noisecutL3_eta23 = {42,41,41,40,41,41,41,41,47,46,44,45,41,42,42,42,43,42,41,39,40,39,39,39,37};
    vector<double> m_noisecutHad_eta23= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,46,60,58,53,55,53,52,51,49,49};

/////------------------------------------------------------------------------------------------
//
// vector<double> m_noisecutPS_eta = {67,67,66,65,66,64,66,66,67,66,68,66,67,66,67,74,75,66, 0, 0, 0, 0, 0, 0,0};
// vector<double> m_noisecutL1_eta = {51,51,52,51,51,50,49,49,43,43,43,42,41,40,43,43,42,45,42,42,45,45,45,42,42};
// vector<double> m_noisecutL2_eta = {56,57,57,56,57,55,54,53,55,54,53,52,50,49,53,48,47,48,56,53,51,50,48,51,50};
// vector<double> m_noisecutL3_eta = {46,45,45,44,45,45,46,48,53,53,54,56,53,46,46,48,48,47,46,45,45,45,45,46,44};
// vector<double> m_noisecutHad_eta= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,60,83,83,73,77,74,71,70,68,65};
// vector<double> eta = { 0.05, 0.15, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 0.85, 0.95, 1.05, 1.15, 1.25, 1.35, 1.45, 1.55, 1.65, 1.75, 1.85, 1.95, 2.05, 2.15, 2.25, 2.35, 2.45};
//
// T2.push_back(eta);
// T2.push_back(m_noisecutHad_eta);
// T2.push_back(eta);
// T2.push_back(m_noisecutPS_eta);
// T2.push_back(eta);
// T2.push_back(m_noisecutL1_eta);
// T2.push_back(eta);
// T2.push_back(m_noisecutL2_eta);
// T2.push_back(eta);
// T2.push_back(m_noisecutL3_eta);
//
// cout<<"---1---"<<endl;
//
// for (int i=1; i<T2.size(); i=i+2) for (int j=0; j<T2[i].size(); j++) {T2[i][j]=(T2[i][j]-32)*25;}



    */
