TFile * F;
TFile *f;
void DrawMCMC(TH1F *h1, TH1F *h2, bool N);


void Compare(TString File, TString h1, TString h2){

f= new TFile(File,"read");
TH1F * H1 = (TH1F*)f->Get(h1);
TH1F * H2 = (TH1F*)f->Get(h2);

DrawMCMC(H1, H2,0);



}



void ComMenus(){

    cout<<"-------------"<<endl;

vector<TString> S;

TFile *file = new TFile ("l1calo_hist.root","read");

TIter next(file->GetListOfKeys());
    TKey* key;
    while ((key = dynamic_cast<TKey*>(next()))) {
        TObject* obj = key->ReadObj();
        if (TString(obj->GetName()).Contains("2023proposedE")) {

            S.push_back(TString(obj->GetName()).Remove(TString(obj->GetName()).Length() - 1, 1));
            std::cout << obj->GetName() << std::endl;
        }
    }


F = new TFile("MenusComparison.root", "recreate");

for (int i=0; i<S.size(); i++) Compare("l1calo_hist.root", S[i]+"E", S[i]+"D");

F->Close();


}




void DrawMCMC(TH1F *h1, TH1F *h2, bool N){

                  if (N==1)
      { h1->Scale(1./h1->Integral());
        h2->Scale(1./h2->Integral());}

    cout<<" -----" << h1->GetName()<<" ------  "<<endl;

    h2->Chi2Test(h1,"UW CHI2 P");


    TCanvas* c1=new TCanvas(h1->GetTitle(), h1->GetTitle(),700,700);

    TPaveText* label = new TPaveText(0.11, 0.72, 0.43, 0.94, "ndc");
	label->SetBorderSize(0);
	label->SetFillStyle(0);
	label->SetTextAlign(13);

	TText* text = label->AddText("#it{ATLAS} #bf{Internal}");
	text->SetTextSize(0.06);
	text = label->AddText("#bf{#sqrt{s}=13 TeV, 139 fb^{-1}}");
	text->SetTextSize(0.045);
	TLegend *leg = new TLegend(0.76, 0.61, 0.89, 0.89);

 	TH1F *H1=(TH1F*)h1->Clone();
	TH1F *H2=(TH1F*)h2->Clone();

	TPad *pad1 = new TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
	pad1->SetTopMargin(0.08);
	pad1->SetBottomMargin(0.02);
	pad1->Draw();
	pad1->cd();

	h1->SetMinimum(0.0);
	h1->SetMaximum(h2->GetMaximum()*1.8);
	h1->SetTitle("");
	h1->Draw("P");
	h1->SetLineColor(kRed);
	h1->SetLineWidth(4);
	h1->SetMarkerColor(kRed);
	h1->SetStats(0.);

	TH1F * h3=(TH1F*)h1->Clone();
	h3->Draw("E2 same");
	h3->SetFillStyle(3004);
	h3->SetFillColor(kRed);

    //h2->SetMarkerStyle(20);
	//h2->SetMarkerColor(kBlack);
	h2->Draw("P same");
	h2->SetLineColor(kGreen);
	h2->SetLineWidth(2);
	h2->SetMarkerColor(kGreen);
	h2->SetStats(0.);

	TH1F * h4=(TH1F*)h2->Clone();
	h4->Draw("E2 same");
	h4->SetFillStyle(3004);
	h4->SetFillColor(kGreen);

	leg->AddEntry(h2,"2023D");
    leg->AddEntry(h1,"2023E");
    leg->SetHeader(h2->GetTitle());
	leg->Draw();
	leg->SetBorderSize(0);
	label->Draw();
	h1->GetXaxis()->SetLabelSize(0.);
	h1->GetYaxis()->SetLabelSize(0.045);
	h1->GetYaxis()->SetTitleOffset(0.8);
	h1->GetYaxis()->SetTitleSize(0.05);

	c1->cd();
	TPad *pad2 = new TPad("pad2","pad2", 0, 0, 1, 0.3);
	pad2->SetBottomMargin(0.4);
	pad2->SetGridy();
	pad2->Draw();
	pad2->cd();
	H1->SetStats(0);
	H1->SetTitle("");

	H1->GetYaxis()->SetRangeUser(0.95, 1.05);
	H1->GetYaxis()->SetTitle("Ratio");
	H1->GetYaxis()->SetTitleSize(0.115);
	H1->GetYaxis()->SetTitleOffset(0.35);
	H1->GetYaxis()->SetLabelSize(0.09);
	H1->GetYaxis()->SetLabelOffset(0.015);
	H1->GetYaxis()->SetNdivisions(4, 1, 0);

	H1->GetXaxis()->SetLabelSize(0.1);
	H1->GetXaxis()->SetLabelOffset(0.03);
	H1->GetXaxis()->SetTitleOffset(1.25);
	H1->GetXaxis()->SetTitleSize(0.11);

	H2->SetMarkerStyle(20);
	H2->Divide(h1);

    for (int i=0; i<H2->GetNbinsX()+2; i++){
        if (h1->GetBinContent(i)==0) H1->SetBinContent(i,0);
        else {H1->SetBinContent(i,1);
             H1->SetBinError(i,h1->GetBinError(i)/h1->GetBinContent(i));}}

	H1->Draw("E2");
	H2->Draw("p same");
	H2->SetLineColor(kBlue);
    H1->SetFillStyle(3004);
	H1->SetFillColor(kRed);
	H1->SetMarkerStyle(1);
	H1->SetMarkerColor(kRed);
	H2->SetMarkerStyle(1);
	H2->SetMarkerColor(kBlue);


//	TH1F * h5=(TH1F*)H2->Clone();
//	h5->Draw("E2 same");
//	h5->SetFillStyle(3004);
//	h5->SetFillColor(kGreen);

	c1->cd();

    string Save(h1->GetName());
    TString SaveAs="Histos/"+Save+".png";

   // f->Close();


   F->cd();

	c1->Write(h1->GetName());
	c1->SaveAs(SaveAs);
	cout<<" -------------------------------------------------  "<<endl;
   }


