void DefineSvectors();
void RunOptimization();
double TriangularIntegral(TH2F *H, double a , double b);
void PrintVectors();
void MakeMultigraph(map<TString, vector<double>> M, vector<double> V, TString Y);
TFile *F;
map<TString, vector<double>> Slop;
map<TString, vector<double>> Slop_old;
map<TString, vector<double>> Int;
map<TString, vector<double>> BkgEffs;
map<TString, vector<double>> BkgEffs_old;
vector<double> ETA(25);




void OptCuts(){

DefineSvectors();

RunOptimization();
//
iota(ETA.begin(), ETA.end(), 0);
//
MakeMultigraph(Slop,ETA,"Slope");
//
MakeMultigraph(Int,ETA,"Intercepts");

MakeMultigraph(BkgEffs,ETA,"Bkg Efficiency");

MakeMultigraph(BkgEffs,ETA,"Slop_old");


PrintVectors();
}


void RunOptimization(){


  //{0.970 ,0.986 ,0.995};
  vector<double> T ={0.970 ,0.986 ,0.995};
  //{0.984, 0.994 ,0.996};
  vector<double> M ={0.984, 0.994 ,0.996};
  //{0.994 ,0.996 ,0.998};
  vector<double> L ={0.994 ,0.996 ,0.998};

 vector <TString> Runs = {"456225","456273","456303","456314","456316","456346","456386","456409","456522","456665","456685","456714","456729","456749"};
 TString path = "/eos/home-p/pbellos/ATLAS/condor/l1caloDaod/20230828.1834_testZeroBias_eFEXIsolation_hadd/";

  TFile *f= new TFile("l1calo_hist_run00452843.root","read");
  TFile *f2= new TFile("ZB_l1calo_hist_run00452843.root","read");
  TString IsoVar[3]={"h_EM_RetaCore_RetaEnv_","h_EM_RhadEm_RhadHad_","h_EM_WstotDen_WstotNum_"};
  TString WP[3]={"T","M","L"};
  TString IsoName[3]={"reta","rhad","wstot"};

  for(int iso=0; iso<3; iso++){

     cout<<"------"<<IsoVar[iso]<<"------"<<endl;



//     TH2F * H3 = (TH2F*)f2->Get(IsoVar[iso]+TString(to_string(0)));
//     TH2F * H2 = (TH2F*)f->Get(IsoVar[iso]+TString(to_string(0)));
//
//     for (int eta=1; eta<25; eta++){  H3 -> Add((TH2F*)f2->Get(IsoVar[iso]+TString(to_string(eta))));}
//     for (int eta=1; eta<25; eta++){  H2 -> Add((TH2F*)f->Get(IsoVar[iso]+TString(to_string(eta))));}

     for (int eta=0; eta<25; eta++){

       TH2F * H3 = (TH2F*)f2->Get(IsoVar[iso]+TString(to_string(eta)));
       TH2F * H2 = (TH2F*)f->Get(IsoVar[iso]+TString(to_string(eta)));

       for(int i=0; i<14; i++){
       TFile *FR= new TFile(path+"l1calo_hist_run00"+Runs[i]+".root","read");
       H3->Add((TH2F*)FR->Get(IsoVar[iso]+TString(to_string(eta))));
       FR->Close();
       }


       double a=0, b=0, X=0,Y=0, MinBkgE[3]={1,1,1};
       int MinBkgI[3];

       TH2F * AB = new TH2F("","",100,0,100,100,0,100);

       vector<double> It, SigE, BkgE, A, B;
       bool G=0;

       double Shift;
     (IsoName[iso].Contains("r")) ? Shift=8.0 : Shift=32.0;

     int Xsteps =  61;
     int Ysteps = 101;
     double Xmin;// = 0;// Shift/Slop_old[IsoName[iso]+"__L"][eta+25] - 0.4 ;

     (IsoName[iso].Contains("r")) ? Xsteps=51 : Xmin=71;

     (IsoName[iso].Contains("r")) ? Xmin=0.0 : Xmin=0.6;

     //Xmin =-0.2;

     //cout<<Xmin<<" --> "<<Shift/Slop_old[IsoName[iso]+"__L"][eta+25]<<endl;



       for (int i=0; i<Xsteps; i++){

           for (int j=0; j<Ysteps; j++){

                a= Xmin + 0.001 + (double)i/100;
                //if (iso==1) a=0.001; //(double)i/50+0.001;
                b= 300 - 5*j;

//                int i=0, j=0;
//               a=1.2; b=100;

                //It.push_back(26*i+j);
                A.push_back(a);
                B.push_back(b);
                SigE.push_back(TriangularIntegral(H2, a, b)/H2->GetEntries());
                BkgE.push_back(TriangularIntegral(H3, a, b)/H3->GetEntries());

//                cout<<"==="<<Slop_old[IsoName[iso]+"__"+WP[i]][eta]<<endl;
//                cout<<"==="<<endl;
//
//                if(b==0 && G==0) {
//
//                  if(a>=Slop_old[IsoName[iso]+"__"+WP[i]][eta]) {
//
//                    BkgEffs_old[IsoName[iso]+"__"+WP[i]].push_back(BkgE.back());
//                    G=1;}}

                //cout<<a<<" "<<b<<" "<<SigE.back()<<" "<<BkgE.back()<<endl;
 }
 }

    for (int i=0;i<A.size(); i++){

          if (SigE[i]>L[iso]){
             if (MinBkgE[2]>BkgE[i])
                  {MinBkgE[2]=BkgE[i]; MinBkgI[2]=i;}}}


    for (int i=0;i<A.size(); i++){

        if (A[i]!=A[MinBkgI[2]]) continue;

        if (SigE[i]>M[iso]){
             if (MinBkgE[1]>BkgE[i])
                  {MinBkgE[1]=BkgE[i]; MinBkgI[1]=i;}}

         if (SigE[i]>T[iso]){
             if (MinBkgE[0]>BkgE[i])
                  {MinBkgE[0]=BkgE[i]; MinBkgI[0]=i;}}

                  }

    for (int i=0; i<3; i++){
     cout<<WP[i]<<" "<<eta<<" "<<SigE[MinBkgI[i]]<<" "<<MinBkgE[i]<<" "<<MinBkgI[i]<<" "<<A[MinBkgI[i]]<<" "<<B[MinBkgI[i]]<<endl;
     Slop[IsoName[iso]+"__"+WP[i]].push_back(A[MinBkgI[i]]);
     Int[IsoName[iso]+"__"+WP[i]].push_back(B[MinBkgI[i]]);
     BkgEffs[IsoName[iso]+"__"+WP[i]].push_back(MinBkgE[i]);
     }
//     BestA.push_back(A[MinBkgI]);
//     BestB.push_back(B[MinBkgI]);

//     TMultiGraph *mg = new TMultiGraph();

 //    TCanvas * C = new TCanvas();
//     TGraph *SE = new TGraph(It.size(),&It[0],&SigE[0]);
//     SE->SetTitle("SE");
//     SE->SetLineColor(kRed);
//     mg->Add(SE,"l");
//
//     TGraph *BE = new TGraph(It.size(),&It[0],&BkgE[0]);
//     BE->SetTitle("SE");
//     BE->SetLineColor(kBlack);
//     mg->Add(BE,"l");
//     mg->Draw("a");

     TH2F * H2Sig = new TH2F("","",Xsteps-1, Xmin, Xmin+((double)Xsteps-1)/100, Ysteps-1, -194, 306);
     TH2F * H2Bkg = new TH2F("","",Xsteps-1, Xmin, Xmin+((double)Xsteps-1)/100, Ysteps-1, -194, 306);

     double contoursSig[3]={T[iso], M[iso], L[iso]};
     double contoursBkg[6]={0.3, 0.4, 0.5, 0.6, 0.7, 0.8};

     for (int i=0;i<A.size(); i++){
       H2Sig->Fill(A[i],B[i],SigE[i]);
       H2Bkg->Fill(A[i],B[i],BkgE[i]);}

     TCanvas * C4 = new TCanvas();
     //H2Sig->DrawCopy("colz");
     H2Sig->GetZaxis()->SetRangeUser(0.5,1);
     H2Sig->SetContour(3,contoursSig);
     H2Sig->Draw("cont3 same");
     H2Sig->SetLineColor(kRed);

     //TCanvas * C5 = new TCanvas();
     //H2Bkg->DrawCopy("colz same");
     H2Bkg->GetZaxis()->SetRangeUser(0.5,1);
     H2Bkg->SetContour(6,contoursBkg);
     H2Bkg->Draw("cont3 same");
     H2Bkg->SetLineColor(kBlack);

     //H2Sig->Draw("cont3 same");

     TLine* line[6];

     for (int i=0; i<3; i++){
     line[i] = new TLine(-B[MinBkgI[i]]/A[MinBkgI[i]], 0, (H2->GetYaxis()->GetXmax()-B[MinBkgI[i]])/A[MinBkgI[i]], H2->GetYaxis()->GetXmax());
//     line[i]->SetBBoxX1(0);
//     line[i]->SetBBoxX2(H2->GetXaxis()->GetXmax());
//     line[i]->SetBBoxY1(H2->GetYaxis()->GetXmax());
//     line[i]->SetBBoxY2(0);
     line[i]->SetLineColor(i+2);}

    // double Shift;
     (IsoName[iso].Contains("r")) ? Shift=8.0 : Shift=32.0;

     for (int i=0; i<3; i++){
     line[i+3] = new TLine(0, 0, H2->GetYaxis()->GetXmax()*Slop_old[IsoName[iso]+"__"+WP[i]][eta+25]/Shift, H2->GetYaxis()->GetXmax());
     line[i+3]->SetLineColor(i+2);
     line[i+3]->SetLineStyle(2);}

//     TLine *old = new TLine (0,0,(H2->GetXaxis()->GetXmax()-0)*124./8., H2->GetYaxis()->GetXmax());
//     old->SetLineColor(kRed);

F= new TFile ("FullStatstest2.root", "update");

     TCanvas * C2 = new TCanvas();
     H2->Draw("hist");
     H2->Write();
     for (int i=0; i<3; i++){line[i]->Draw("same");}

     TCanvas * C3 = new TCanvas();
     H3->Draw("hist");
     H3->Write();
     for (int i=0; i<3; i++){line[i]->Draw("same");}



     //C->Write();
     C2->Write();
     C3->Write();
     C4->Write();
     //C5->Write();

     string Save(H2->GetName());
     TString SaveAs="Histos/"+Save+"_sig.png";

     string Save2(H3->GetName());
     TString SaveAs2="Histos/"+Save2+"_bkg.png";

     TString SaveAs3="Histos/"+Save2+"_SigBkg.png";

	//c1->Write(h1->GetName());
    C2->SaveAs(SaveAs);
	C3->SaveAs(SaveAs2);
	C4->SaveAs(SaveAs3);
	F->Close();

     }

//     TCanvas * C3 = new TCanvas();
//     H3->Draw("hist");
//     string Save2(H3->GetName());
//     TString SaveAs2="Histos/"+Save2+"_bkg .png";
//
//     TCanvas * C2 = new TCanvas();
//     H2->Draw("hist");
//      string Save(H2->GetName());
//     TString SaveAs="Histos/"+Save+"_Sig.png";
//
//
//	C2->SaveAs(SaveAs);
//	C3->SaveAs(SaveAs2);



     }}







double TriangularIntegral(TH2F *H, double a , double b){
  double integral=0;
  for (int x_bin = H->GetXaxis()->FindBin(-b/a); x_bin <= H->GetNbinsX()+1; x_bin++) {
       // cout<<x_bin<<" --> "<<"0-"<<H->GetYaxis()->FindBin(a*x_bin*H->GetXaxis()->GetXmax()/(H->GetNbinsX()-2)+b)<<" "<<integral/H->GetEntries()<<endl;
        for (int y_bin = 0; y_bin <=H->GetYaxis()->FindBin(a*x_bin*H->GetXaxis()->GetXmax()/(H->GetNbinsX()-2)+b); y_bin++) {
            integral += H->GetBinContent(x_bin, y_bin);
            }}

  return integral;}




void MakeMultigraph(map<TString, vector<double>> M, vector<double> V, TString Y){

  F= new TFile ("FullStatstest2.root", "update");

  TMultiGraph *MG = new TMultiGraph();
  vector<TGraph*> GR;

  int i=-1;

  for ( map<TString, vector<double>>::iterator it = Slop.begin(); it != Slop.end(); it++) {
     i++;
     GR.push_back(new TGraph(it->second.size(),&V[0],&M[it->first][0]));
     GR.back()->SetTitle(it->first);
     GR.back()->SetMarkerColor(i%3+2);
     GR.back()->SetMarkerStyle(i/3+24);
     MG->Add(GR.back());}


 TCanvas *C = new TCanvas();
 MG->Draw("AP");
 C->BuildLegend();
 C->Write();

 F->Close();

}



void PrintVectors(){

double Shift;

 for (const auto& [key, value] : Slop){
        std::cout<<" std::vector<unsigned int> "  << key << "_S = {";
        (key.Contains("r")) ? Shift=8.0 : Shift=32.0;
      for(int i=0; i<2*value.size()-1; i++){
          if (i==value.size()-1) cout<< static_cast<int>(Shift/(value[fabs((int)value.size()-i-1)])) << " , ";
            cout<<static_cast<int>(Shift/(value[fabs((int)value.size()-i-1)])) << " , ";}
        cout<<"};"<<endl;}

for (const auto& [key, value] : Int){
        std::cout<<" std::vector<unsigned int> " << key << "_I = {";
        (key.Contains("r")) ? Shift=1.0 : Shift=1.0;
      for(int i=0; i<2*value.size()-1; i++){
          if (i==value.size()-1) cout<< static_cast<int>(Shift*(value[fabs((int)value.size()-i-1)]))+10000 << " , ";
            cout<<static_cast<int>(Shift*(value[fabs((int)value.size()-i-1)]))+10000 << " , ";}
        cout<<"};"<<endl;}

}



void DefineSvectors(){
Slop_old["reta__L"]={70, 67, 79, 62, 76, 83, 67, 78, 63, 68, 15, 64, 78, 79, 89, 73, 84, 71, 87, 93, 99, 95, 76, 82, 88, 88, 82, 76, 95, 99, 93, 87, 71, 84, 73, 89, 79, 78, 64, 15, 68, 63, 78, 67, 83, 76, 62, 79, 67, 70};
Slop_old["rhad__L"] ={219, 157, 148, 126, 135, 129, 125, 108, 94, 132, 118, 168, 184, 199, 182, 121, 113, 99, 116, 91, 104, 90, 95, 98, 81, 81, 98, 95, 90, 104, 91, 116, 99, 113, 121, 182, 199, 184, 168, 118, 132, 94, 108, 125, 129, 135, 126, 148, 157, 219};
Slop_old["wstot__L"] ={8, 37, 47, 38, 39, 45, 47, 45, 43, 42, 8, 38, 32, 32, 32, 32, 32, 39, 41, 36, 38, 37, 36, 37, 38, 38, 37, 36, 37, 38, 36, 41, 39, 32, 32, 32, 32, 32, 38, 8, 42, 43, 45, 47, 45, 39, 38, 47, 37, 8};
Slop_old["reta__M"] = {78, 79, 92, 76, 87, 99, 100, 102, 89, 85, 59, 84, 104, 102, 115, 101, 123, 105, 124, 133, 131, 128, 119, 123, 130, 130, 123, 119, 128, 131, 133, 124, 105, 123, 101, 115, 102, 104, 84, 59, 85, 89, 102, 100, 99, 87, 76, 92, 79, 78};
Slop_old["rhad__M"] = {250, 186, 176, 160, 162, 135, 152, 134, 127, 152, 187, 219, 250, 250, 223, 138, 142, 122, 140, 114, 120, 118, 119, 121, 107, 107, 121, 119, 118, 120, 114, 140, 122, 142, 138, 223, 250, 250, 219, 187, 152, 127, 134, 152, 135, 162, 160, 176, 186, 250};
Slop_old["wstot__M"] = {8, 40, 50, 42, 45, 47, 49, 48, 44, 43, 8, 41, 38, 34, 32, 37, 32, 39, 42, 39, 40, 40, 38, 38, 38, 38, 38, 38, 40, 40, 39, 42, 39, 32, 37, 32, 34, 38, 41, 8, 43, 44, 48, 49, 47, 45, 42, 50, 40, 8};
Slop_old["reta__T"] = {98, 89, 99, 90, 99, 115, 115, 119, 103, 102, 81, 101, 123, 119, 134, 126, 141, 133, 148, 164, 157, 151, 152, 157, 156, 156, 157, 152, 151, 157, 164, 148, 133, 141, 126, 134, 119, 123, 101, 81, 102, 103, 119, 115, 115, 99, 90, 99, 89, 98};
Slop_old["rhad__T"] = {250, 250, 235, 223, 215, 199, 189, 171, 155, 199, 250, 250, 250, 250, 250, 183, 183, 157, 190, 159, 163, 169, 158, 146, 150, 150, 146, 158, 169, 163, 159, 190, 157, 183, 183, 250, 250, 250, 250, 250, 199, 155, 171, 189, 199, 215, 223, 235, 250, 250};
Slop_old["wstot__T"] = {8, 44, 55, 49, 51, 50, 54, 50, 47, 49, 8, 46, 45, 44, 43, 43, 43, 45, 45, 42, 42, 42, 41, 41, 41, 41, 41, 41, 42, 42, 42, 45, 45, 43, 43, 43, 44, 45, 46, 8, 49, 47, 50, 54, 50, 51, 49, 55, 44, 8};
}










