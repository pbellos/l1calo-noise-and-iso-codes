Noise thresholds 
===========================

Make LAr ntuples
-----------------

export AtlasSetup=/afs/cern.ch/atlas/software/dist/AtlasSetup
source $AtlasSetup/scripts/asetup.sh --input /afs/cern.ch/user/l/larcomm/w0/digitMon/LArCAF_22.0.102/asetupConfig
source /afs/cern.ch/user/l/larcomm/w0/digitMon/LArCAF_22.0.102/build/x86_64-centos7-gcc11-opt/setup.sh

LArSCDump_tf.py  --CA --inputBSFile /eos/atlas/atlastier0/rucio/data23_13p6TeV/physics_ZeroBias/00451094/data23_13p6TeV.00451094.physics_ZeroBias.merge.RAW/data23_13p6TeV.00451094.physics_ZeroBias.merge.RAW._lb0500._SFO-ALL._0001.1 --outputNTUP_SCMONFile test.root



Clone and run Chonghao's code to calculate thresholds
---------------------------------

https://gitlab.cern.ch/chonghao/efexnoisecut

root -b Analysis.cpp



Run Plotting code 
-------------------------

root PlotThresholds.cpp




Slope + Intercept iso
==================================

Calculate 2D isolation thresholds
-----------------------------------------

root Opt.Cuts.cpp


Compare menu perfornance 
-------------------------------

root ComMenus.cpp










